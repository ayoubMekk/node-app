const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send({ node_app: "this app is deployed and piplined with gitlab CI/CD" });
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});